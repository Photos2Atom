#!/bin/sh
# see http://purl.mro.name/Photos2Atom
#
cd "$(dirname "${0}")" || exit 1

if [ "" = "${2}" ] ; then
	# Should we enforce that $${2} is a url?
cat <<Endofmessage

Extract meta data from a set of images and write the photo feed atom xml to stdout.

SYNOPSIS

  $0 <Feed Name> <baseurl> [src/image.jpg]...

Endofmessage
	exit 1
fi

title="${1}"
shift
base="${1}"
shift

if [ "$(date -d '@123' --iso-8601=seconds 2>/dev/null)" = "1970-01-01T01:02:03+01:00" ] ; then
	file_date_iso8601 () {
		date -r "${1}" --iso-8601=seconds
	}
elif [ "$(date -r 123 +'%FT%T%z' | sed 's/..$/:&/g' 2>/dev/null)" = "1970-01-01T01:02:03+01:00" ] ; then
	file_date_iso8601 () {
		date -r "${1}" +'%FT%T%z' | sed 's/..$/:&/g'
	}
else
	file_date_iso8601 () {
		'1970-01-01T00:00:00+00:00'
	}
fi

file_size () {
	wc -c < "${0}" | tr -d ' '
}

youngest=$(ls -tr "$@" | tail -n 1)

cat <<Endofmessage
<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type='text/xsl' href='assets/atom2html.xslt'?>
<!--
	https://developer.mozilla.org/en/docs/XSL_Transformations_in_Mozilla_FAQ#Why_isn.27t_my_stylesheet_applied.3F

	Note that Firefox will override your XSLT stylesheet if your XML is
	detected as an RSS or Atom feed. A known workaround is to add a
	sufficiently long XML comment to the beginning of your XML file in
	order to "push" the <.feed> or <.rss> tag out of the first 512 bytes,
	which is analyzed by Firefox to determine if it's a feed or not. See
	the discussion on bug
	https://bugzilla.mozilla.org/show_bug.cgi?id=338621#c72 for more
	information.


	For best results serve both atom feed and xslt as 'text/xml' or
	'application/xml' without charset specified.
-->
<feed xmlns="http://www.w3.org/2005/Atom" xmlns:media="http://search.yahoo.com/mrss/" xmlns:georss="http://www.georss.org/georss">
 <title><![CDATA[${title}]]></title>
 <generator uri="http://purl.mro.name/Photos2Atom">Photos2Atom</generator>
 <id>${base}/</id>
 <updated>$(file_date_iso8601 "${youngest}")</updated>
 <link href="${base}/" rel="self" type="application/atom+xml"/>
 <link href="${base}/" rel="alternate" type="text/html"/>
 <author>
  <name>John Doe</name>
  <uri>http://example.com/~jd</uri>
 </author>
Endofmessage

while [ "" != "${1}" ]
do
	src="${1}"
	file="$(basename "${src}")"
	mime="$(file --brief --mime-type "${src}")"
	size="$(file_size "_build/200/${file}"y%)"

	cat <<Endofmessage
 <entry>
  <id>${base}/#${file}</id>
  <updated>$(file_date_iso8601 "${src}")</updated>
  <title></title>
  <summary/>
  <content src="${base}/1200/${file}" type="${mime}"/>
  <media:thumbnail url="${base}/200/${file}"/>
  <!-- georss:point>47.874091670000006 12.639475000000001</georss:point -->
  <link href="${base}/200/${file}" rel="previewimage" length="${size}" type="${mime}"/>
  <link href="${base}/1200/${file}" rel="enclosure" length="$(file_size "_build/1200/${file}")" type="${mime}"/>
  <link href="${base}/1200/${file}" type="${mime}"/>
 </entry>
Endofmessage
  shift
done

cat <<Endofmessage
</feed>
Endofmessage
