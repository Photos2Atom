#
# Example
#
# $ make title='My Photos' base='https://example.com/sub/my-photos'
#
# See http://purl.mro.name/Photos2Atom
#

.PHONY: all build clean large thumb

_build/200/%: src/%
	@-mkdir -p _build/200
	@# https://www.smashingmagazine.com/2015/06/efficient-image-resizing-with-imagemagick/
	@# https://www.linuxquestions.org/questions/linux-software-2/question-about-thumbnail-orientation-dimensions-with-imagemagick-493034/
	convert $< -thumbnail 200x200  -quality 30% -auto-orient -strip -define png:exclude-chunk=all -interlace Plane $@
	@touch -r "$<" "$@"

_build/1200/%: src/%
	@-mkdir -p _build/1200
	convert $< -resize 1200x1200\> -quality 82% -auto-orient -strip -define png:exclude-chunk=all -interlace Plane $@
	@touch -r "$<" "$@"

build: _build/index.xml

all: build

clean:
	rm -rf _build

SRC := $(wildcard src/*)

thumb: $(patsubst src/%,_build/200/%,${SRC})
large: $(patsubst src/%,_build/1200/%,${SRC})

_build/index.xml.raw: thumb large
	sh ./atom.sh '$(title)' '$(base)' ${SRC} > $@

_build/index.xml: rfc4287.rng _build/index.xml.raw
	xmllint --format --encode utf-8 --nocdata --nonet --output $@ --relaxng $^
