<?xml version="1.0" encoding="UTF-8"?>
<!--
  iPhoto2Atom, extract images from iPhoto™ libraries
  Copyright (C) 2015-2016  Marcus Rohrmoser, http://purl.mro.name/iPhoto2Atom

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.


  Turn a atom photo feed into a html preview page.


  http://www.w3.org/TR/xslt/
-->
<xsl:stylesheet
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:a="http://www.w3.org/2005/Atom"
  xmlns:media="http://search.yahoo.com/mrss/"
  xmlns:georss="http://www.georss.org/georss"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="a media georss"
  version="1.0">

  <!-- replace linefeeds with <br> tags -->
  <xsl:template name="linefeed2br">
    <xsl:param name="string" select="''"/>
    <xsl:param name="pattern" select="'&#10;'"/>
    <xsl:choose>
      <xsl:when test="contains($string, $pattern)">
        <xsl:value-of select="substring-before($string, $pattern)"/><br class="br"/><xsl:comment>Why do we see 2 br on Safari and output/@method=html here? http://purl.mro.name/safari-xslt-br-bug</xsl:comment>
        <xsl:call-template name="linefeed2br">
          <xsl:with-param name="string" select="substring-after($string, $pattern)"/>
          <xsl:with-param name="pattern" select="$pattern"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="degrees">
    <xsl:param name="num" select="0"/>
    <xsl:choose>
      <xsl:when test="$num &lt; 0">-<xsl:call-template name="degrees"><xsl:with-param name="num" select="-$num"/></xsl:call-template></xsl:when>
      <xsl:when test="$num &gt;= 0">
        <xsl:variable name="deg" select="floor($num)"/>
        <xsl:variable name="min" select="floor(($num * 60) mod 60)"/>
        <xsl:variable name="sec" select="format-number((($num * 36000) mod 600) div 10, '0.0')"/>
        <xsl:value-of select="$deg"/>° <!--
        --><xsl:value-of select="$min"/>' <!--
        --><xsl:value-of select="$sec"/>"
      </xsl:when>
      <xsl:otherwise>?</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:output
    method="html"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>

  <!-- http://stackoverflow.com/a/16328207 -->
  <xsl:key name="CategorY" match="a:entry/a:category" use="@term" />

  <xsl:template match="a:feed">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta content="text/html; charset=utf-8" http-equiv="content-type"/>
        <!-- https://developer.apple.com/library/IOS/documentation/AppleApplications/Reference/SafariWebContent/UsingtheViewport/UsingtheViewport.html#//apple_ref/doc/uid/TP40006509-SW26 -->
        <!-- http://maddesigns.de/meta-viewport-1817.html -->
        <!-- meta name="viewport" content="width=device-width"/ -->
        <!-- http://www.quirksmode.org/blog/archives/2013/10/initialscale1_m.html -->
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <!-- meta name="viewport" content="width=400"/ -->
        <!-- http://lokeshdhakar.com/projects/lightbox2/#how-to-use -->
        <link href="assets/lightbox2/css/lightbox.min.css" rel="stylesheet" />
        <link href="assets/style.css" rel="stylesheet" type="text/css"/>
        <title>
          <xsl:value-of select="a:title"/>
        </title>
        <link href="photos.atom" rel="alternate" type="application/atom+xml"/>
      </head>
      <body id="photofeed">
        <h1><xsl:value-of select="a:title"/></h1>

        <ul id="photos">
          <xsl:for-each select="a:entry">
            <xsl:sort select="a:updated" order="descending"/>
            <li id="{substring-after(a:id, '#')}">
              <xsl:attribute name="class">
                <xsl:for-each select="a:category">cat_<xsl:value-of select="generate-id(key('CategorY',@term))"/><xsl:text> </xsl:text></xsl:for-each>
              </xsl:attribute>

              <xsl:variable name="time" select="substring(translate(a:updated,'T',' '), 1, 16)"/>
              <a href="{a:link[@rel='enclosure']/@href}" title="{a:updated} {normalize-space(a:title)}" data-lightbox="foo" data-title="{normalize-space(a:title)} | {$time}">
                <img alt="{normalize-space(a:summary)}" src="{a:link[@rel='previewimage']/@href}"/>
              </a>
              <span style="display:none">
                <span class="title"><xsl:value-of select="a:title"/></span><br class='br'/>
                <span class="time"><xsl:value-of select="$time"/></span><br class='br'/>
                <xsl:variable name="lat" select="substring-before(georss:point, ' ')"/>
                <xsl:variable name="lon" select="substring-after(georss:point, ' ')"/>
                gps: <a class="geo" title="{$lat},{$lon}" href="http://www.openstreetmap.org/?mlat={$lat}&amp;mlon={$lon}#map=13/{$lat}/{$lon}&amp;layers=C">
                  <!-- xsl:value-of select="georss:point"/ -->
                  <xsl:call-template name="degrees"><xsl:with-param name="num" select="$lat"/></xsl:call-template>
                  <xsl:text> </xsl:text>
                  <xsl:call-template name="degrees"><xsl:with-param name="num" select="$lon"/></xsl:call-template>
                </a><br class='br'/>
                <span class="categories">
                  <xsl:for-each select="a:category">
                    <a onclick="toggleCategory(this)" class="category cat_{generate-id(key('CategorY',@term))}"><xsl:value-of select="@label"/></a>,
                  </xsl:for-each>
                  .
                </span>
              </span>
            </li>
          </xsl:for-each>
        </ul>

        <p style="clear:both">
          <xsl:for-each select="a:entry/a:updated">
            <xsl:sort select="." order="ascending"/>
            <xsl:if test="position() = 1"><xsl:value-of select="substring(., 1, 10)"/></xsl:if>
          </xsl:for-each>
          -
          <xsl:for-each select="a:entry/a:updated">
            <xsl:sort select="." order="descending"/>
            <xsl:if test="position() = 1"><xsl:value-of select="substring(., 1, 10)"/></xsl:if>
          </xsl:for-each>,
          #<xsl:value-of select="count(a:entry)"/>
        </p>

      <div style="display:none">
        <h2 style="clear:left">Schlagworte</h2>

        <ul id="categories" class="categories">
          <!-- http://stackoverflow.com/a/16328207 -->
          <xsl:for-each select="a:entry/a:category[generate-id() = generate-id(key('CategorY',@term))]">
            <xsl:sort select="@label"/>
            <xsl:sort select="@term"/>
            <li id="{generate-id(key('CategorY',@term))}">
              <a onclick="toggleCategory(this)" class="category cat_{generate-id(key('CategorY',@term))}"><xsl:value-of select="@label"/></a><xsl:text> </xsl:text><xsl:value-of select="count(../../a:entry[a:category/@term = current()/@term])"/>
            </li>
          </xsl:for-each>
        </ul>
      </div>

        <!-- http://lokeshdhakar.com/projects/lightbox2/#how-to-use -->
        <script src="assets/lightbox2/js/lightbox-plus-jquery.min.js" type="text/javascript"></script>
        <script src="assets/script.js" type="text/javascript"></script>

        <p id="footer" style="clear:both">
          Powered by <a href="http://purl.mro.name/Photos2Atom">purl.mro.name/Photos2Atom</a>
          and <a rel="license" href="https://creativecommons.org/licenses/by/2.5/"><img alt=
  "Creative Commons License" src=
  "assets/by.svg" style="border:0;width:88px;height:31px"/></a>
  <span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">lightbox2</span> by <a xmlns:cc=
  "http://creativecommons.org/ns#" href="http://lokeshdhakar.com/projects/lightbox2/" property=
  "cc:attributionName" rel="cc:attributionURL">Lokesh Dhakar</a> is licensed under a <a rel=
  "license" href="https://creativecommons.org/licenses/by/2.5/">Creative Commons Attribution 2.5 License</a>.
          <br class="br"/>
          <a title="Validate my Atom 1.0 feed" href="https://validator.w3.org/feed/check.cgi?url={a:link[@rel='self']/@href}">
          <img alt="Valid Atom 1.0" src="assets/valid-atom.png" style="border:0;width:88px;height:31px"/>
          </a><xsl:text> </xsl:text>
          <a href="https://validator.w3.org/check?uri=referer">
          <img alt="Valid XHTML 1.0 Strict" src="assets/valid-xhtml10-blue-v.svg" style="border:0;width:88px;height:31px"/>
          </a>
          <a href="https://jigsaw.w3.org/css-validator/check/referer?profile=css3&amp;usermedium=screen&amp;warning=2&amp;vextwarning=false&amp;lang=de">
          <img alt="CSS ist valide!" src="assets/valid-css-blue-v.svg" style="border:0;width:88px;height:31px"/>
          </a>
          <!-- https://i.creativecommons.org/l/by/2.5/88x31.png -->
          </p>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
