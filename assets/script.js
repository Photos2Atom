/*
  iPhoto2Atom, extract images from iPhoto™ libraries
  Copyright (C) 2015-2016  Marcus Rohrmoser, http://purl.mro.name/iPhoto2Atom

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var active = {};

function toggleCategory(elem) {
  var m = elem.getAttribute('class').match(/cat_[^ ]+/); 
  if( m ) {
    var clz = '.' + m[0];
    console.log(clz);
    if( active[clz] ) {
      $(clz).removeClass('active');
      delete active[clz];
    } else {
      $(clz).addClass('active');
      active[clz] = true;
    }
    if( 0 == Object.keys(active).length ) {
      $('#photos').removeClass('active');
    } else {
      $('#photos').addClass('active');
    }
  }
}
