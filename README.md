
Prepare a static photo feed for publication on a webserver.

# Usage

    $ # put the photos or softlinks into ./src/, then
    $ make title='My Photos' base='https://example.org/sub/my-photos'

    $ # optional:
    $  sh ./install-lightbox2.sh
    $  rsync -aP assets _build/

    $ rsync -avPz _build/ example.org:/var/www/.../

## Prerequisites

- [make](https://www.gnu.org/software/make/)
- [imagemagick](https://imagemagick.org/)
- [xmllint](http://xmlsoft.org/xmllint.html)
- [rsync](http://rsync.samba.org/)
- [curl](https://curl.haxx.se/)

## Appendix

- Atom
  - http://atomenabled.org/developers/syndication/
  - https://tools.ietf.org/html/rfc4287#appendix-B
- Yahoo media extensions
  - http://www.rssboard.org/media-rss
- GeoRSS
  - http://www.geonames.org/rss-to-georss-converter.html

## Mirrors

see doap.rdf
