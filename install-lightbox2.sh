#!/bin/sh
#
# iPhoto2Atom, extract images from iPhoto™ libraries
# Copyright (C) 2015-2016  Marcus Rohrmoser, http://purl.mro.name/iPhoto2Atom
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Download and install http://lokeshdhakar.com/projects/lightbox2/
#

src="https://github.com/lokesh/lightbox2/archive/v2.8.2.zip"
dst="assets/lightbox2"

cd "$(dirname "$0")"

zip=".tmp/lightbox2.zip"

curl --location --create-dirs --remote-time --time-cond "${zip}" --output "${zip}" --url "${src}"

rm -rf "${dst}"
for d in css images js
do
  mkdir -p "${dst}/${d}"
  unzip -jo "${zip}" */dist/"${d}"/*.* -d "${dst}/${d}"
done
echo "-------------------"
find "${dst}" -name .DS_Store -exec rm "{}" \;
find "${dst}" -type f \( -name *.js -or -name *.css \) -not -name *.min.* -exec rm "{}" \;
find "${dst}" -type f -name *.map -or -name lightbox.min.js -exec rm "{}" \;

rm -rf "$(dirname "${zip}")"
find "${dst}" -type f
