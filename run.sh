#!/bin/sh
cd "$(dirname "${0}")" || exit 1

make title='My Photos' base='https://example.org/sub/my-photos'

dst="example.org:/var/www/vhosts/example.org/pages/sub/my-photos"
rsync -avP _build/ "${dst}/"
rsync -avP assets/ "${dst}/assets/"
